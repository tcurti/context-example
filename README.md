# Context View

## Table of Contents

1. [About the Context View](#about-the-context-view)
2. [Concerns](#concerns)
3. [Scope](#scope)
4. [Relevant Stakeholders](#relevant-stakeholders)
5. [Brief Description](#brief-description)
6. [Runtime Context](#runtime_context)
7. [Project Context](#project_context)
8. [Appendices](#appendices)

## About the Context View

This view is meant as an introduction to this AD, and as such provides a brief description of Mindustry as well as of its environment.

## Concerns

Serving as an introduction to this AD, this view's concerns include defining the system's responsibilities, identifying the external services with which Mindustry interacts (as well as each's purpose), how these services allow the system to assume its functions, and how Mindustry adaps to its environment (ie, the various platforms it can be played on.)

## Scope

This view explores the services Mindustry interacts with to the extent permissible by the information available on its open-source repository. As such, [features that come with a paid version of the game](#extra), and whose source code is external to the repository, are deemed out of scope.

## Relevant Stakeholders

This view can be useful to most stakeholders in some fashion or other, as it provides a high-level overview of what Mindustry can do and what external services it requires. As such, it can be helpful in understanding the system at a basic level (sufficient for nontechnical stakeholders such as acquirers & interested users) and provides an entry point to the AD for more technical stakeholders (such as devs.)

## Brief description

Mindustry is a sandbox-type videogame revolving around the acquisition of resources using machines, and the production of fighting units to attack enemies (and of further harvesting machines) using said resources. Mindustry can be played on any of the major platforms (PC, Apple computers & mobile devices, Android, Linux), and offers cross-platform online multiplayer, meaning that users on different platforms can play together. This Context view aims to give an overview of the various external services used by Mindustry to function. These services fit into two sets : those actively relied upon by Mindustry at runtime to deliver its various functions, and those necessary to further the development of Mindustry.


## <a name="runtime_context">Diagram 1 : Runtime Context</a>

This is the **runtime context diagram** for the system. Only includes elements with which the game interacts during its execution.

![Runtime context diagram](resources/context_view_runtime.png)

### Element catalog

- **Mindustry Game Application (System of interest)**. Responsibilities include keeping track of the game’s state, updating it according to internal events or player actions transmitted by the Arc framework, and transmitting these changes to the framework. The system must remain sufficiently responsive at all times, to ensure the player does not notice delays between their input and the displayed output. The system must also be intuitive to use, and notify the player (who is also the operator) of any errors that they may be able to correct themselves (eg current graphics card is insufficient) with sufficient precision and at an appropriate level of abstraction for them to understand.
    - <a name="extra">Note</a> that the version of the game as purchased via Steam contains additional functionality, such as an achievement list, a more user-friendly interface for selecting servers and using mods, and cloud saves[^2]. As this functionality is not part of the open-source repository on GitHub (and its code is likely located somewhere in the Steam service), it is deemed out of scope for this project. However, as the code responsible for “inserting” the game into its Steam context is part of the OSS[^4], it is included as part of the linking service (see below) for this analysis.
- **The player** represents the main user of this service, who will download and run the application. As the application is available publicly, users of this system are not limited to a specific group and can include anyone.
- **The linking service** to general gaming-related platforms. This corresponds to the APIs allowing the system to optionally insert itself into the frameworks provided by the Steam game distribution service and the Discord messaging/streaming service, letting the player's accounts with those services benefit from added functionality related to the Mindustry system. Notably, Mindustry informs these platforms of the game’s activity (ie whether the system is running)[^1] and of other information which the platform displays in its own fashion (eg achievements completed, servers available to join, save data)[^2]. Note that this service is exclusive to desktop-launched instances of the system[^4].
- **Client machine file system**. The computer on which the system is run also doubles as a data store which contains numerous assets (maps, sprites, fonts, music, save data etc.) essential to the game’s proper functioning[^3]. Assets are regularly loaded from (eg maps) and saved to (eg saves) this store, and take up ~200Mb in total[^5].
- **Platform-specific game launching service**. This service groups the launchers[^4]<sup>,</sup>[^6]<sup>,</sup>[^7] responsible for wrapping the running game into a process specific to the platform (iOS, Android, or Desktop) in use, and getting the process running on the platform (ie providing the game system with resources such as memory, CPU, etc.) Once the launcher service is finished setting up the necessary environment, the game is launched on the same VM. Which one of the three launchers is used depends on the platform. Selection of the appropriate launcher is performed (manually) at packaging time[^8]; hence automatically by the user at download time (if acquired from a distribution service, see 2nd diagram below), or manually by the user if not acquired through a distribution service.
- **Game application framework** (Arc). This entity groups several services :
    - **The I/O application framework** [^9] represents the standard application framework services that bridge the device’s input/output interfaces and the system. Its responsibilities are mostly delegation-based : it transmits input events (keyboard, mouse, touchscreen uses) to the system, and the system’s output (sound, graphics, camera control, error messages) to the device (OS). As such, requests to its services are frequent and must be processed efficiently (though this may be limited, in part, by the OS’s resources.)
    - **The server launching service** is responsible for creating a headless instance (ie dedicated server) of the game on the device, to which other clients should be able to connect via the Network service (see below), and providing a handle via which it can be controlled[^10]<sup>,</sup>[^13]. This service is only used during dedicated-server multiplayer games (see the [information view](information_view.md#multiplayer) for details), and only by the server instance.
    - **The network service** This represents the interface via which the system uses the computer's Internet connection to communicate (both sending & receiving) with the server (or the clients, if the system instance is a server) during online games. Client systems sends information such as the player’s inputs, and receives information such as the game’s current state (shared across players.)[^11]<sup>,</sup>[^12]
    This service must be fast and responsive, as frequent exchanges of data between multiple devices are essential to keep the various players’ game states consistent. This service is only used during multiplayer games, by all system instances instances involved.
    - **The external asset store** provides general (non-Mindustry-specific) assets (fonts), in addition to the assets already included as part of the system[^14]<sup>,</sup>[^15]. This service is used regardless of the type of game played.
- **Mod Libraries**. The system provides an API for players to use unofficial, custom mods with the game. These can provide additional assets or modify the behaviour of the system.[^16]<sup>,</sup>[^17] If a mod is detected, the data it contains is loaded.
- (Omitted) **Processing devices/Operating systems**. The OS/physical device on which Mindustry is running is also an important entity with which the game interacts; additionally, this OS can take various forms, from mobile (Android/iOS) to non-mobile (Mac, Windows, Linux etc.). However, all these interactions occur via either the launching service or the game framework, and as such OSes are abstracted as part of these entities.

## <a name="project_context">Diagram 2 : Project context</a>

This is the **non-runtime context diagram** for the system. Includes entities important to the system’s lifecycle, but with which it does not interact at runtime.

![Static context diagram](resources/context_view_static.png)

### Element catalog

- **Mindustry Project**. In this context, the game system represents the project on which development is ongoing. An important aspect of it is the code itself; as such, its main responsibility is to represent the application that is provided to players. The Project is the main product the developers work with, extend, and use various tools on (listed below.)
- **Testing framework**. This service’s responsibilities include providing a way to easily automate the running of tests as well as an efficient mechanism to assess test failures. Its ease-of-use and speed of execution are key quality properties, as developers typically have little time or patience for testing.
- **App development management & code hosting service**. This service’s responsibilities involve providing storage for the codebase as well as interfaces permitting multiple developers to contribute to the project (this includes downloading the code and managing the uploading and inclusion of changes made to it.) Key quality properties in this case include availability (notably, protection against hardware failures), ease of use, and security (a strict control of contributors’ permissions, to ensure only approved changes get through.)
- **Application building service**. Responsibilities include downloading, managing and providing developers with access to the required dependencies (code libraries), as well as packaging the application code and dependencies into multiple binary executables compatible with and optimized for the supported platforms (iOS/Android/etc.)
- **Game distribution services**.[^8] These each host packaged, executable versions of the application, which users can download and run. Each distribution service may only offer versions of the game for a subset of Mindustry’s supported platforms, but together they cover all of them. They may or may not charge the user a fee for the download.

## Insight acquisition

The wide majority of insights presented here were acquired by looking at the package structure, manually browsing the code, and tracking down uses of each Gradle dependency (and occasionally looking up the nature of a particular library.)

## Appendices

[^1]: Discord Rich Presence [https://discord.com/rich-presence](https://discord.com/rich-presence)

[^2]: Mindustry FAQ [https://mindustrygame.github.io/wiki/faq/](https://mindustrygame.github.io/wiki/faq/)

[^3]: Mindustry Assets [https://github.com/Anuken/Mindustry/tree/master/core/assets](https://github.com/Anuken/Mindustry/tree/master/core/assets)

[^4]: Desktop Launcher [https://github.com/Anuken/Mindustry/blob/master/desktop/src/mindustry/desktop/DesktopLauncher.java](https://github.com/Anuken/Mindustry/blob/master/desktop/src/mindustry/desktop/DesktopLauncher.java)

[^5]: Mindustry Steam page [https://store.steampowered.com/app/1127400/Mindustry/](https://store.steampowered.com/app/1127400/Mindustry/)

[^6]: iOS Launcher [https://github.com/Anuken/Mindustry/blob/master/ios/src/mindustry/ios/IOSLauncher.java](https://github.com/Anuken/Mindustry/blob/master/ios/src/mindustry/ios/IOSLauncher.java)

[^7]: Android Launcher [https://github.com/Anuken/Mindustry/tree/master/android/src/mindustry/android](https://github.com/Anuken/Mindustry/tree/master/android/src/mindustry/android)

[^8]: Mindustry Readme [https://github.com/Anuken/Mindustry/blob/master/README.md](https://github.com/Anuken/Mindustry/blob/master/README.md)

[^9]: Arc framework https://github.com/Anuken/Arc

[^10]: Server files [https://github.com/Anuken/Mindustry/tree/master/server/src/mindustry/server](https://github.com/Anuken/Mindustry/tree/master/server/src/mindustry/server)

[^11]: Main network class [https://github.com/Anuken/Mindustry/blob/master/core/src/mindustry/net/Net.java](https://github.com/Anuken/Mindustry/blob/master/core/src/mindustry/net/Net.java)

[^12]: Arc Net submodule [https://github.com/Anuken/Arc/tree/master/extensions/arcnet/src/arc/net](https://github.com/Anuken/Arc/tree/master/extensions/arcnet/src/arc/net)

[^13]: Arc Server submodule [https://github.com/Anuken/Arc/blob/master/backends/backend-headless/src/arc/backend/headless/HeadlessApplication.java](https://github.com/Anuken/Arc/blob/master/backends/backend-headless/src/arc/backend/headless/HeadlessApplication.java)

[^14]: Arc font usage [https://github.com/Anuken/Mindustry/blob/master/core/src/mindustry/ui/Fonts.java](https://github.com/Anuken/Mindustry/blob/master/core/src/mindustry/ui/Fonts.java)

[^15]: Arc font submodule [https://github.com/Anuken/Arc/tree/master/extensions/freetype/src/arc/freetype](https://github.com/Anuken/Arc/tree/master/extensions/freetype/src/arc/freetype)

[^16]: Mindustry Modding guide [https://mindustrygame.github.io/wiki/modding/1-modding/](https://mindustrygame.github.io/wiki/modding/1-modding/#hjson)

[^17]: Main system’s mod entry point [https://github.com/Anuken/Mindustry/tree/master/core/src/mindustry/mod](https://github.com/Anuken/Mindustry/tree/master/core/src/mindustry/mod)
